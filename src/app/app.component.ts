import { Component } from '@angular/core';
import Utils from './utils'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Prototüüp';

  logIn(): void {
    window.location.href = 'http://localhost:3000/lhv/auth'
  }

  isLoggedIn() {
    return !!Utils.getCookie('lhv_access_token');
  }

  logOut() {
    Utils.deleteCookie('lhv_access_token');
    Utils.deleteCookie('lhv_refresh_token');
    Utils.deleteCookie('lhv_consent_id');
  }
}
