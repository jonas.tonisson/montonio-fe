export interface Account {
  iban: string,
  currency: string,
  name: string,
  status: string
}
