import { Component, OnInit } from '@angular/core';
import axios from "axios";
import Utils from "../utils";
import { Account } from "./account";
import { environment as e } from '../../environments/environment';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.less']
})
export class AccountsComponent implements OnInit {

  accounts = <Account[]>[];
  account = '';
  paymentResponse = '';

  constructor() { }

  ngOnInit(): void {
    if (!Utils.getCookie('lhv_consent_id')) {
      this.getLhvUserConsent();
    }
    this.getLhvAccounts();
  }

  getLhvUserConsent() {
    axios.get( e.montonio_api_url + '/lhv/consent', {withCredentials: true}).then(response => {
      Utils.setCookie('lhv_consent_id', response.data.consentId);
      window.location.href = response.data._links.scaRedirect.href;
    });
  }

  getLhvAccounts() {
    axios.get( e.montonio_api_url + '/lhv/accounts', {withCredentials: true}).then(response => {
      this.accounts = response.data.accounts;
    });
  }

  pay(account: string) {
    axios.post( e.montonio_api_url + '/lhv/pay', account, {
      withCredentials: true
    }).then(response => {
      this.paymentResponse = JSON.stringify(response.data);
    });
  }
}
